import React from 'react';
import BlockItem from "./BlockItem";
import {CSSTransition, TransitionGroup} from "react-transition-group";

const BlockList = ({blocks, title, remove}) => {

    if (!blocks.length) {
        return (
            <h3 style={{textAlign: 'center', marginTop: '20px'}}>
                Blocks are not found!
            </h3>
        )
    }
    return (
        <div>
            <h1 style={{textAlign: "center"}}>
                {title}
            </h1>

            <TransitionGroup>
                <div style={{
                    display: "flex", flexDirection: "row", columnGap: "25px",
                     flexWrap: "wrap", justifyContent:"center"
                }}>
                    {blocks.map((block, index) =>
                        <CSSTransition
                            key={block.id}
                            timeout={500}
                            classNames='block'
                        >
                            <BlockItem remove={remove} number={index + 1} block={block}/>
                        </CSSTransition>
                    )}
                </div>
            </TransitionGroup>


        </div>
    );
};

export default BlockList;