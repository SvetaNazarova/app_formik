import {useMemo} from "react";

export const useSortedBlocks =(blocks, sort)=> {
    const sortedBlocks = useMemo(() => {
        if (sort) {
            return [...blocks].sort((a, b) => a[sort].localeCompare(b[sort]))

        }
        return blocks;
    }, [sort, blocks])
    return sortedBlocks;

}

export const useBlocks=(blocks,sort,query)=>{
    const sortedBlocks=useSortedBlocks(blocks,sort);

    const sortedAndSearchedBlocks = useMemo(() => {
        return sortedBlocks.filter(block => block.title.toLowerCase().includes(query))

    }, [query, sortedBlocks])
    return sortedAndSearchedBlocks;

}