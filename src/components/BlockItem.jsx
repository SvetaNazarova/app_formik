import React from 'react';
import MyButton from "./UI/button/MyButton";


const BlockItem = (props) => {
    return (
        <div className='block'>
            <div className= 'block__content'>
                <div>
                    <img src="" alt=""/>
                    {props.block.file}
                </div>
                <strong>{props.number}. {props.block.title}</strong>
                <div>
                    {props.block.body}
                </div>
                <div>
                    {props.block.text}
                </div>
            </div>
            <div className='block__btns'>
                <MyButton onClick={() => props.remove(props.block)} >Delete</MyButton>

            </div>
        </div>
    );
};

export default BlockItem;