import React, {useEffect, useMemo, useRef, useState} from "react";
import MyButton from "./components/UI/button/MyButton";
import MyInput from "./components/UI/input/MyInput";
import './styles/App.css'
import BlockItem from "./components/BlockItem";
import BlockList from "./components/BlockList";
import BlockForm from "./components/UI/Blockform/BlockForm";
import MySelect from "./components/UI/select/MySelect";
import BlockFilter from "./components/BlockFilter";
import MyModal from "./components/UI/MyModal/MyModal";
import {useBlocks} from "./hooks/useBlocks";
import SignUpForm from "./components/signUpForm";


function App() {
    const [blocks, setBlocks] = useState([])
    const [filter, setFilter] = useState({sort: '', query: ''})
    const [modal, setModal] = useState(false);
    const sortedAndSearchedBlocks = useBlocks(blocks, filter.sort, filter.query);

    const createBlock = (newBlock) => {
        setBlocks([...blocks, newBlock])
        setModal(false)

    }

    const removeBlock = (block) => {
        setBlocks(blocks.filter(b => b.id !== block.id))
    }

    return (
        <div className="App">
            <MyButton style={{marginTop: '30px'}} onClick={() => setModal(true)}>
                Create block
            </MyButton>
            <MyModal visible={modal} setVisible={setModal}>
                <BlockForm create={createBlock}/>
            </MyModal>

            <hr style={{margin: '15px 0'}}/>
            <BlockFilter
                filter={filter}
                setFilter={setFilter}
            />
            <BlockList remove={removeBlock} blocks={sortedAndSearchedBlocks} title="Block list"/>


        </div>
    );
}

export default App;
