import React, {useState} from 'react';
import MyInput from "../input/MyInput";
import MyButton from "../button/MyButton";

const BlockForm = ({create}) => {
    const [block, setBlock] = useState({title: '', body: '', text: ''})

    const addNewBlock = (e) => {
        e.preventDefault()
        const newBlock = {
            ...block, id: Date.now()

        }
        create(newBlock)
        setBlock({title: '', body: '', text: '', file: ''})
    }

    return (
        <form style={{display: 'flex', flexDirection: 'column'}}>
            <div id="upload-container" method="POST" action="send.php">
                <MyInput value={block.file}
                     id="profile_pic"
                     name="profile_pic"
                     accept=".jpg, .jpeg, .png"
                     type="file"
                     onChange={e => setBlock({...block, file: e.target.value})}
                     multiple>
                </MyInput>
                </div>

            <MyInput
                value={block.title}
                onChange={e => setBlock({...block, title: e.target.value})}
                type="text"
                maxlength="20"
                placeholder="title"/>
            <MyInput
                value={block.body}
                onChange={e => setBlock({...block, body: e.target.value})}
                type="url" placeholder='url'/>
            <MyInput
                value={block.text}
                onChange={e => setBlock({...block, text: e.target.value})}
                type="text"
                placeholder="text"/>

            <MyButton onClick={addNewBlock}>New block</MyButton>
        </form>
    );
};

export default BlockForm;
